from flask import Flask,render_template,request
import os
app = Flask(__name__)

#Constants for our error codes
STATUS_OK = 200
STATUS_FORBIDDEN = 403
STATUS_NOT_FOUND = 404
STATUS_NOT_IMPLEMENTED = 401

#Error handler for files that are not found in the templates folder
@app.errorhandler(STATUS_NOT_FOUND)
def error_404():
    return render_template("404.html"), STATUS_NOT_FOUND

#Error handler for forbidden chars like '..', '//' and '~'
@app.errorhandler(STATUS_FORBIDDEN)
def error_403():
    return render_template("403.html"), STATUS_FORBIDDEN

#This function is used to look at every path that the user enters in the browser
@app.route("/", defaults={'path': ''})
@app.route('/<path:path>')
def pages(path):

    #This is checking for the home page if the user just opened 127.0.0.1:5000\
    if (path == ''):
        return """This is the Home Page :: 
                  Majed Almazrouei, malmazr2@uoregon.edu"""

    #This to check for forbidden chars in the path like '\~trivia.html'
    elif ("//") in path or ("~") in path or ("..") in path:

        return error_403()

    #This checks for html and css files in the templates folder.
    #I included the css search in this project in order for the
    #the server to read both html files and css files that are at
    #the same folder.
    #I also created a static->styles folder and it is also working
    elif (path.endswith(".html") or path.endswith(".css")):

        #This is a try and except function, i used it so if the user asked
        #for a file that does not exit in the folder, i return error 404 for it
        try:
            
            return render_template(path), STATUS_OK
        
        except:

            return error_404()
        
    #This else statement is to return a not implemented error similar to the first project.
    else:
            
        return ("Not Implemented"), STATUS_NOT_IMPLEMENTED

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
